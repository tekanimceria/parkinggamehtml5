function Trotoar(width,height){
	if(width==undefined){width=40;}
	if(height==undefined){height=15;}
	this.x=0;
	this.y=0;
	this.rotation=0;
	this.vx=0;
	this.vy=0;
	this.width=width;
	this.height=height;
	//this.color="#000000";
}

Trotoar.prototype.draw=function(context){
	context.save();
	
	context.translate(this.x, this.y);
    context.rotate(this.rotation);
    context.scale(this.scaleX, this.scaleY);
    //context.fillStyle = this.color;
    context.beginPath();
    context.moveTo(this.x,this.y);
    context.lineTo(this.width,this.y);
    context.lineTo(this.width,this.height);
    context.lineTo(this.x,this.height);
    context.lineTo(this.x,this.y);
    context.closePath();
    //context.fill();  
    
	context.restore();
}

function convertDegree(value){
	var newValue = (value*180/Math.PI).toFixed(0)%360;
	if(newValue < 0){
		newValue += 360;
	}
	return newValue;
}

Trotoar.prototype.getBounds=function(){
    if(convertDegree(this.rotation) == 0 || convertDegree(this.rotation) == 2){ 
        return{
            x: this.x,
            y: this.y,
            width: this.width,
            height: this.height
        }
    }
    else{
        return{
            x: this.x,// - this.height/2,
            y: this.y,// - this.width/2,
            width: this.height,
            height: this.width
        }
    }
}