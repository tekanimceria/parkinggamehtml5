// $(document).ready(function(){
//     $("#main").click(function(){
//         $("#kotak-menu").hide();
//         $("#kotak-menu-stage").show();
//     });

//     $("#logo-home").click(function(){
//         $("#kotak-menu-stage").hide();
//         $("#kotak-menu").show();

//     });
// });

window.onload = function(){
    document.getElementById("main").onclick = function(){
        document.getElementById("kotak-menu").style.display = "none";
        document.getElementById("kotak-menu-stage").style.display = "block";        
    }
    document.getElementById("logo-home").onclick = function(){
        document.getElementById("kotak-menu").style.display = "block";
        document.getElementById("kotak-menu-stage").style.display = "none";        
    }

    if (typeof(Storage) !== "undefined") {
        // Code for localStorage/sessionStorage.
        for(var i = 0; i < 5; i++){
            var stage = "stage_" + (i+1);
            if(localStorage.getItem(stage) == null){
                localStorage.setItem(stage,"0");
            }
            else{
                var jumBintang = localStorage.getItem(stage);
                for(var j = 0; j < jumBintang; j++){
                    var img = document.getElementById("bintang_"+ (i+1) + "_" + (j+1));
                     img.src = "pendukung/asset/bintang-i.png";
                }
            }
        }
    
    } else {
        // Sorry! No Web Storage support..
        alert("Maaf, data skor permainan anda tidak" +
        " bisa disimpan karena browser anda tidak mendukung localStorage");
    }

    var modal = document.getElementById('popup-bantuan');
    var btn = document.getElementById("bantuan");
    var span = document.getElementsByClassName("close")[0];
    var kotakMenu = document.getElementById("kotak-menu");
    var kotakMenuStage = document.getElementById("kotak-menu-stage");
    var btnMain = document.getElementById("main");
    var btnHome = document.getElementById("logo-home");

    btn.onclick = function() {
        modal.style.display = "block";
    }

    span.onclick = function() {
        modal.style.display = "none";
    }

    btnMain.onclick = function(){
        kotakMenuStage.style.display = "block";
        kotakMenu.style.display = "none"
    }

    btnHome.onclick = function(){
        kotakMenuStage.style.display = "none";
        kotakMenu.style.display = "block";
    }

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}

function openStage(i){
    switch(i){
        case 1: window.location = "stage1.html";break;
        case 2: window.location = "stage2.html";break;
        case 3: window.location = "stage3.html";break;
        case 4: window.location = "stage4.html";break;
        case 5: window.location = "stage5.html";break;           
    }
}

function resetSkor(){
    if(confirm("Apa anda yakin ingin menghapus semua skor anda?")){
        for(var i = 0; i < 5; i++){
            var stage = "stage_" + (i+1);
            localStorage.setItem(stage,"0");
        }

        alert("Skor dihapus!");
        window.location.reload();
    }

}
