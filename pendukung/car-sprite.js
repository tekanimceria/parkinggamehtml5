function Car(width,height,img){
	if(width==undefined){width=40;}
	if(height==undefined){height=15;}
	this.hp=1000;
	this.x=0;
	this.y=0;
	this.rotation=0;
	this.vx=0;
	this.vy=0;
	this.ax=0;
	this.ay=0;
	this.v=0;
	this.a=0;
	this.gigi=1;
	this.ban=0;
	this.stir=0;
	this.vMaks=10;
	this.width=width;
	this.height=height;
	this.img=img;
}

Car.prototype.draw=function(context){
	context.save();
	//ban
	context.translate(this.x,this.y);
	context.rotate(this.rotation);
	context.translate(this.width/1.5,-this.height/2 + 5);
	context.rotate(this.ban);
	context.beginPath();
	context.rect(-12.5,-5,22,10);
	context.rotate(-this.ban);
	context.translate(0,this.height-10);
	context.rotate(this.ban);
	context.rect(-12.5,-5,22,10);
	context.rotate(-this.ban);
	context.closePath();	
	context.fill();		
	context.restore();
	//mobil
	context.save();
	context.translate(this.x,this.y);
	context.rotate(this.rotation);	
	context.translate(-this.width/10,-this.height/2);
	context.drawImage(this.img,0,0,this.width,this.height);
	context.restore();
}

// function convertDegree(value){
// 	var newValue = (value*180/Math.PI).toFixed(0)%360;
// 	if(newValue < 0){
// 		newValue += 360;
// 	}
// 	return newValue;
// }

function rotate(xCenter,yCenter,x,y,angle){
	var	cos = Math.cos(angle), 
		sin = Math.sin(angle);

	var dx = x - xCenter,
		dy = y - yCenter;
			
	x = cos * dx - sin * dy,
	y = cos * dy + sin * dx;

	x += xCenter;
	y += yCenter;

	return{
		xNew: x,
		yNew: y
	}
}

// function minMax(data){
// 	var lowest = 99999, highest = 0;
// 	// console.log(data);
// 	data.forEach(element => {
// 		if(element < lowest){
// 			lowest = element;
// 		}
		
// 		if(element > highest){
// 			highest = element;
// 		}
// 	});

// 	return{
// 		lowest: lowest,
// 		highest: highest
// 	}
// }

function indexOfMin(arr) {
    if (arr.length === 0) {
        return -1;
    }

    var min = arr[0];
    var minIndex = 0;

    for (var i = 1; i < arr.length; i++) {
        if (arr[i] < min) {
            minIndex = i;
            min = arr[i];
        }
    }

    return minIndex;
}


function findFirstPointIdx(xs,ys){
	// var topX =;
	var idxFirstY = indexOfMin(ys);
	var tempYs = ys.slice();
	tempYs[idxFirstY] = Infinity;
	var idxSecondY = indexOfMin(tempYs);
	
	var topLeftIdx = idxFirstY;
	if(xs[idxSecondY] < xs[idxFirstY]){
		topLeftIdx = idxSecondY;
	}
	
	return topLeftIdx;
}

Car.prototype.getBounds=function(){
	//kiri atas
	var x1 = this.x - this.width/10,
		y1 = this.y - this.height/2,
	//kanan atas
		x2 = x1 + this.width,
		y2 = y1,
	//kanan bawah
		x3 = x1 + this.width,
		y3 = y1 + this.height,
	//kiri bawah
		x4 = x1,
		y4 = y1 + this.height;

	var result = rotate(this.x,this.y,x1,y1,this.rotation);
	x1 = result.xNew;
	y1 = result.yNew;

	result = rotate(this.x,this.y,x2,y2,this.rotation);
	x2 = result.xNew;
	y2 = result.yNew;

	result = rotate(this.x,this.y,x3,y3,this.rotation);
	x3 = result.xNew;
	y3 = result.yNew;

	result = rotate(this.x,this.y,x4,y4,this.rotation);
	x4 = result.xNew;
	y4 = result.yNew;

	var xs = [x1,x2,x3,x4],
		ys = [y1,y2,y3,y4];

	var idx = findFirstPointIdx(xs,ys);
	
	var tempXs = [],
		tempYs = [];

	for(var i = 0; i < 4; i++){
		tempXs.push(xs[idx]);
		tempYs.push(ys[idx]);	
		idx++;
		if(idx > 3){
			idx = 0;
		}
	}

	return{
		x1: tempXs[0],
		y1: tempYs[0],
		x2: tempXs[1],
		y2: tempYs[1],
		x3: tempXs[2],
		y3: tempYs[2],
		x4: tempXs[3],
		y4: tempYs[3]
	}	
}