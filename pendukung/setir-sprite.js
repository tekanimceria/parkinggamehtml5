function Setir(radius, img){
	if(radius==undefined){radius=150;}
	this.x=0;
	this.y=0;
	this.rotation=-90*Math.PI/180;
	this.radius=radius;
	this.img=img;
	this.kuadran4 = false;
	this.kuadran1 = false;
	this.stirValue = 0;
	this.putaranSetir = 0;
}

Setir.prototype.putar=function(dx, dy){
	var angle=Math.atan2(dy,dx);
	var degrees = angle*(180/Math.PI);

	if (dx>0 && dy>0){ // kuadran 2
		degrees+=90;
		this.kuadran1 = false;
		this.kuadran4 = false;
	} else if (dx>=0 && dy<=0){ // kuadran 1
		if (this.kuadran4) {
			this.putaranSetir++;
			this.kuadran4=false;
		}
		this.kuadran1 =true;
		degrees+=90;
	} else if (dx<0 && dy<0){ // kuadran 4
		if (this.kuadran1) {
			this.putaranSetir--;
			this.kuadran1=false;
		}
		this.kuadran4 = true;
		degrees+=450;
	} else if (dx<=0 && dy>=0){ //kuadran 3
		degrees+=90;
		this.kuadran1 = false;
		this.kuadran4 = false;
	}

	//hitung putaran setir
	if (this.putaranSetir<0) {
		degrees -= 360*(this.putaranSetir*-1);
	}
	else if (this.putaranSetir>0) {
		degrees += 360*this.putaranSetir;
	}
	this.stirValue = degrees;
}

Setir.prototype.draw=function(context){
	context.save();
	context.translate(this.x,this.y);
	context.rotate(this.rotation);
	context.translate(-this.radius/2,-this.radius/2);
	context.globalAlpha=0.7;	
	context.drawImage(this.img,0,0,this.radius,this.radius);
	context.restore();
}