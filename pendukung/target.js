function Target(width,height,img) {
    if(width==undefined){
        width = 100;
    }
    if(height==undefined){
        height = 175;
    }
    this.x = 0;
    this.y = 0;
    this.vx = 0;
    this.vy = 0;
    this.rotation = 0;
    this.scalex = 0;
    this.scaley = 0;
    this.width = width;
    this.height = height;
    this.color = "#beff9b";
    this.lineColor = "#1aff1a";
    this.lineWidth = 10;
    this.img = img;
}

Target.prototype.draw=function(context){
    context.save();

    context.translate(this.x, this.y);
    context.rotate(this.rotation);
    context.translate(-this.width/2,-this.height/2);
    context.drawImage(this.img,0,0,this.width,this.height);
    //context.scale(this.scaleX, this.scaleY);
    // context.lineWidth = this.lineWidth;
    // context.fillStyle = this.color;
    // context.strokeStyle = this.lineColor;
    // context.beginPath();
    // context.moveTo(-this.width/2,-this.height/2);
    // context.lineTo(this.width/2,-this.height/2);
    // context.lineTo(this.width/2,this.height/2);
    // context.lineTo(-this.width/2,this.height/2);
    // context.lineTo(-this.width/2,-this.height/2);
    // context.closePath();
    // context.fill();  
    // context.stroke();
    context.restore();
}

function convertDegree(value){
	var newValue = (value*180/Math.PI).toFixed(0)%360;
	if(newValue < 0){
		newValue += 360;
	}
	return newValue;
}

Target.prototype.getBounds=function(){
    if(convertDegree(this.rotation) == 0 || convertDegree(this.rotation) == 2){ 
        return{
            x: this.x - this.width/2,
            y: this.y - this.height/2,
            width: this.width,
            height: this.height
        }
    }
    else{
        return{
            x: this.x - this.height/2,
            y: this.y - this.width/2,
            width: this.height,
            height: this.width
        }
    }
}